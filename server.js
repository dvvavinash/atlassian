const mockService = require('osprey-mock-service')
const express = require('express')
const path = require('path')
const cors = require('cors')


let app

mockService.loadFile(path.join(__dirname, 'space-api.raml'))
  .then((mockApp) => {
    app = express()
    app.use(cors())
    app.use(mockApp)
    app.listen(3001)
    console.log('server is listens at 3001')
  })
  .catch(e => {
    console.log(e);
  })