import merge from 'lodash/merge';

const browser = typeof window !== 'undefined';

const config = {
  // defaults
  all: {
    env: process.env.NODE_ENV || 'developement',
    publicPath: '/',
    apiUrl: 'http://localhost:3001',
    versionString: 'v0.0.1',
    apiVersion: 'api/v1',
    browser,
  },
  developement: {
    apiUrl: 'http://localhost:3001',
  },
  test: {},
  production: {}
};

export default  {...merge(config.all, config[config.all.env])};
