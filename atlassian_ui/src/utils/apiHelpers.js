// Request utils,
// feel free to replace with your code
// (get, post are used in ApiServices)
import 'whatwg-fetch';


/**
 * 1. parse response
 * 2. add "ok" property to result
 * 3. add "status" property to result
 * 3. return request result
 * @param  {Object} res - response from server
 * @return {Object} response result with "ok" property
 */
async function parseJSON(res) {
  let json;

  try {
    json = await res.json();
  } catch (e) {
    return {
      data: {},
      ok: false,
      status: 0,
    };
  }

  // simplest validation ever, ahah :)
  if (!res.ok) {
    return {
      data: json,
      ok: false,
      status: res.status,
    };
  }

  // resultOK - is a function with side effects
  // It removes ok property from result object
  return {
    data: json,
    ok: true,
    status: res.status,
  };
}


// FUNCTION WITH SIDE-EFFECTS
/**
 * `parseJSON()` adds property "ok"
 * that identicates that response is OK
 *
 * `resultOK`removes result.ok from result and returns "ok" property
 *  It widely used in `/actions/*`
 *  for choosing action to dispatch after request to API
 *
 * @param  {Object} result - response result that
 * @return {bool} - indicates was request successful or not
 */
export function resultOK(result) {
  if (result) {
    const ok = result.ok;

    delete result.ok; // eslint-disable-line

    return ok; // look at parseJSON
  }

  return false;
}

function requestWrapper(method) {
  return async (url, data = null, params = {}) => {
    if (method === 'GET') {
      // is it a GET?
      // GET doesn't have data
      params = data; // eslint-disable-line
      data = null; // eslint-disable-line
    } else if (data === Object(data)) {
      data = JSON.stringify(data); // eslint-disable-line
    } else {
      throw new Error(`XHR invalid, check ${method} on ${url}`);
    }

    // default params for fetch = method + (Content-Type)
    const headers = {
      'Content-Type': 'application/json; charset=UTF-8',
    };


    const defaults = {
      method,
      headers
    };
    if (data) {
      defaults.body = data;
    }

    const paramsObj = {
      ...defaults,
      headers: {
        ...defaults.headers,
        ...params,
      }
    };

    return fetch(url, paramsObj).then(parseJSON).catch(err => {
      console.error(err); // eslint-disable-line
    });
  };
}

export const get = requestWrapper('GET');
export const post = requestWrapper('POST');
export const put = requestWrapper('PUT');
export const patch = requestWrapper('PATCH');
export const del = requestWrapper('DELETE');

