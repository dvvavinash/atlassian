import config from '../config';
import {
  get,
} from './apiHelpers.js';

let { apiUrl } = config;


if (!apiUrl) apiUrl = '';

export const getAllSpaces = () => get(`${apiUrl}/space`);
export const getCurrentSpaceDetails = (id) => get(`${apiUrl}/space/${id}`);
export const getCurrentSpaceEntries = (id, ) => get(`${apiUrl}/space/${id}/entries`);
export const getCurrentSpaceEntryDetails = (id, entryId) => get(`${apiUrl}/space/${id}/entries/${entryId}`);
export const getCurrentSpaceAssets = (id, ) => get(`${apiUrl}/space/${id}/entries`);
export const getCurrentSpaceAssetDetails = (id, assetId) => get(`${apiUrl}/space/${id}/entries/${assetId}`);
export const getCurrentUsers = (id, ) => get(`${apiUrl}/users`);
export const getCurrentUserDeatils = (userId) => get(`${apiUrl}/users/${userId}`);