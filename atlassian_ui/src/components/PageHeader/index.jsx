import React from 'react';

import { Header } from 'semantic-ui-react';

import './styles.css';

const PageHeader = ({ spaceData, usersNames }) => (
  <div className="spacepage-header-container">
    <Header as='h1'>
      {spaceData && spaceData.fields &&spaceData.fields.title}
      <span>Created by {spaceData && spaceData.sys && usersNames && usersNames[spaceData.sys.createdBy]}</span>
    </Header>
  </div>
)

export default PageHeader;