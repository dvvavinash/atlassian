import React from 'react';

import { Table, List, Button, Icon, Header } from 'semantic-ui-react';

import moment from 'moment';
import { map, isEqual } from 'lodash';
import classNames from 'classnames';

import messages from '../../constants/messages.json';
import { ENTRIES, ASSETS, DATETIME } from '../../constants/default.js';

import './styles.css';
class DataTable extends React.Component {
  static getDerivedStateFromProps(nextProps, state) {
    if (
      !isEqual(nextProps.entries, state.entries) || 
      !isEqual(nextProps.assets, state.assets) || 
      !isEqual(nextProps.users, state.users)
    ) {
      return {
        ...state,
        entries: nextProps.entries,
        assets: nextProps.assets,
        users: nextProps.users,
      }
    }
    return null;
  }
  constructor(props) {
    super(props);
    this.state = {
      entries: props.entries,
      assets: props.assets,
      users: props.users,
      currentTab: ENTRIES,
      sort: {
        type: null,
        order: false,
      }
    }
  }
  updateTab = (type) => () => this.setState({ currentTab: type });
  renderArrows = (type, fieldType) => {
    return(
      <Button basic icon onClick={() => this.sortData(type, fieldType)}> 
        <Icon name="sort" />
      </Button> 
    )
  }
  sortSpacesObjects = (list, type, fieldType, isDate) => {
    if (isDate) return list.sort((a, b) => (a[fieldType][type] > b[fieldType][type]) - (a[fieldType][type] < b[fieldType][type]));
    return list.sort((a, b) => (moment(a[fieldType][type]) > moment(b[fieldType][type])) - (moment(a[fieldType][type]) < moment(b[fieldType][type])));
  }
  sortData = (type, fieldType) => {
    const { currentTab, entries, assets, users, sort } = this.state;
    if (currentTab === ENTRIES) {
      const newEntries = this.sortSpacesObjects(entries, type, fieldType, type === 'updatedAt');
      this.setState({
        entries: sort.order ? newEntries : newEntries.reverse(),
        sort: {
          type,
          order: !sort.order
        }
      })
    } else {
      const newAssets = this.sortSpacesObjects(assets, type, fieldType, type === 'updatedAt');
      this.setState({
        assets: newAssets,
        sort: {
          type,
          order: !sort.order
        }
      })
    }
  }
  render() {
    const { entries, assets, users, currentTab } = this.state;
    const data = currentTab === ENTRIES ? entries : assets;
    return (
      <div className="data-table-container">
        <List horizontal>
          <List.Item>
            <List.Content>
              <List.Header>
                <Button basic className={classNames({ activeTab: currentTab === ENTRIES })} onClick={this.updateTab(ENTRIES)}> Entries </Button>
              </List.Header>
            </List.Content>
          </List.Item>
          <List.Item>
            <List.Content>
              <List.Header>
                <Button basic className={classNames({ activeTab: currentTab === ASSETS })} onClick={this.updateTab(ASSETS)}> Assets </Button>
              </List.Header>
            </List.Content>
          </List.Item>
        </List>
        {
          !data 
          ? <Header as="h4">{messages.no_data_avalable}</Header>
          : <Table celled>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Title{this.renderArrows('title', 'fields')}</Table.HeaderCell>
                  <Table.HeaderCell>Summary{this.renderArrows('summary', 'fields')}</Table.HeaderCell>
                  <Table.HeaderCell>Created By{this.renderArrows('createdBy', 'sys')}</Table.HeaderCell>
                  <Table.HeaderCell>Updated By{this.renderArrows('updatedBy', 'sys')}</Table.HeaderCell>
                  <Table.HeaderCell>Last By{this.renderArrows('updatedAt', 'sys')}</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {
                  map(data, ({ fields, sys }, idx) => (
                    <Table.Row key={idx}>
                      <Table.Cell>{fields.title}</Table.Cell>
                      <Table.Cell> {fields.summary}</Table.Cell>
                      <Table.Cell>{users && users[sys.createdBy]}</Table.Cell>
                      <Table.Cell>{users && users[sys.updatedBy]}</Table.Cell>
                      <Table.Cell>{moment(sys.updatedAt).format(DATETIME)}</Table.Cell>
                    </Table.Row>
                  ))
                }
              </Table.Body>
            </Table>
        }
      </div>
    )
  }
}

export default DataTable;
