import React from 'react';

import {
  Image 
} from 'semantic-ui-react';

import images from '../../constants/images';

import './styles.css';
console.log(images)
const Header = () => (
  <div className="header-container">
    <Image size="small" src={images.logo} alt="atlassian"/>
  </div>
);


export default Header;