export const PAGE_NAMES = {
  SPACE_ONE: 'My First Space',
  SPACE_TWO: 'My Second Space',
  SPACE_THREE: 'My Third Space',
}

export const getPageNamesAsArray = Object.values(PAGE_NAMES);