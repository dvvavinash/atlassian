import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';

import './styles/index.css';

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();

// if any error message is occer we can catch them and send them to the logging server
// browser won't show any message on this methods due to security reasons
/*
  message: error message (string). Available as event (sic!) in HTML onerror="" handler.
  source: URL of the script where the error was raised (string)
  lineno: Line number where error was raised (number)
  colno: Column number for the line where the error occurred (number)
  error: Error Object (object)
*/
window.onerror = function(message, source, lineno, colno, error) { 
  // xhr call
}