import React from 'react';
import { Link } from 'react-router-dom';
import {
  Loader,
  Message
} from 'semantic-ui-react';

import classNames from 'classnames';
import { map } from 'lodash';

import { getAllSpaces } from '../../utils/api';
import { resultOK } from '../../utils/apiHelpers';
import messages from '../../constants/messages';

import Header from '../../components/Header';

import { createBrowserHistory } from 'history';

import './styles.css';
const history = createBrowserHistory();

class SideMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      listOfSpaces: [],
      isError: false,
    }
  }
  componentDidMount() {
    this.getAllSpaces()
  }

  getAllSpaces = async () => {
    this.setState({
      isLoading: true
    })
    const res = await getAllSpaces();
    if (resultOK(res)) {
      this.setState({
        listOfSpaces: res.data.items,
        isLoading: false,
      });
      history.push(`/spaceexpolorer/${res.data.items[0].sys.id}`);
      return;
    } else {
      this.setState({
        isError: true,
        isLoading: false,
      })
      return;
    }
    this.setState({
      isLoading: false,
    })
  }
  render() {
    const { listOfSpaces, isLoading, isError } = this.state;
    const { paramId } = this.props;
    if (isLoading) {
      return (
        <Loader active/>
      );
    }
    return (
      <div className="side-menu-container">
        <Header />
        {
          isError &&
            <Message negative>
              <Message.Header>We're sorry something went wrong.</Message.Header>
              <p>{messages.errors.sidemenu}</p>
            </Message>
        }
        <ul>
         {
           !isError && !isLoading &&map(listOfSpaces, ({ fields, sys }) => 
            <li key={sys.id}>
              <Link className={classNames({ active: paramId === sys.id })}  to={`/spaceexpolorer/${sys.id}`}> {fields.title} </Link>
            </li>
          )
         }
        </ul>
      </div>
    );
  }
}

export default SideMenu;