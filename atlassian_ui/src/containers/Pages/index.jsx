import React from 'react';
import { Grid, Loader, Message } from 'semantic-ui-react';

import { map } from 'lodash';

import { 
  getCurrentSpaceDetails, 
  getCurrentSpaceEntries,
  getCurrentSpaceAssets,
  getCurrentUsers,
} from '../../utils/api';
import { resultOK } from '../../utils/apiHelpers';
import messages  from '../../constants/messages.json';

import SideMenu from '../SideMenu';
import DataTable from '../../components/DataTable';
import PageHeader from '../../components/PageHeader';
import './styles.css';

class Pages extends React.Component {
  constructor() {
    super()
    this.state = {
      currentSpaceData: {},
      isLoading: false,
      errorData: { // don't need to show all error messages if more than one service fail
        type: null,
        message: null
      }
    }
  }

  componentDidMount() {
    this.getCurrentSpaceDetails(this.props.match.params.id);
  }

  getCurrentSpaceDetails = async (id) => {
    this.setState({
      isLoading: true
    });
    const currentSpaceData = await getCurrentSpaceDetails(id);
    if (resultOK(currentSpaceData)) {
      this.setState({
        currentSpaceData: currentSpaceData.data,
      });
    } else {
      this.setState({
        errorData: {
          type: 'space',
          message: messages.errors.space
        }
      });
    }

    const entitiesData = await getCurrentSpaceEntries(id);
    if (resultOK(entitiesData)) {
      this.setState({
        entitiesData: entitiesData.data,
      });
    } else {
      this.setState({
        errorData: {
          type: 'entities',
          message: messages.errors.entities
        }
      });
    }

    const assetsData = await getCurrentSpaceAssets(id);
    if (resultOK(assetsData)) {
      this.setState({
        assetsData: assetsData.data
      });
    } else {
      this.setState({
        errorData: {
          type: 'assets',
          message: messages.errors.assets
        }
      });
    }

    const usersData = await getCurrentUsers();
    if (resultOK(usersData)) {
      const usersNames = {};
      map(usersData.data.items, ({ fields, sys }) => {
        usersNames[sys.id] = fields.name
      });
      this.setState({
        usersNames,
        usersData: usersData.data
      });
    } else {
      this.setState({
        errorData: {
          type: 'users',
          message: messages.errors.users
        }
      });
    }
    this.setState({
      isLoading: false,
    });
  }

  renderErrorMessage = (errorData) => {
    return (
      <Message negative>
        <Message.Header>We're sorry something went wrong.</Message.Header>
        <p>{errorData.message}</p>
      </Message>
    )
  }

  render() {
    const { entitiesData, assetsData, usersNames, currentSpaceData, isLoading, errorData } = this.state;
    return (
      <div  className="pages-container">
        <Grid celled='internally'>
          <Grid.Row>
            <Grid.Column width={3} className="side-menu-wrapper">
              <SideMenu paramId={this.props.match.params.id}/>
            </Grid.Column>
            <Grid.Column width={13}> 
              {
                isLoading
                ? <Loader active/>
                : <React.Fragment>
                    {errorData.type && this.renderErrorMessage(errorData)}
                    <PageHeader spaceData={currentSpaceData} usersNames={usersNames} />
                    <DataTable
                      entries={entitiesData && entitiesData.items}
                      assets={assetsData && assetsData.items}
                      users={usersNames}
                    />
                  </React.Fragment>
              }
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    )
  }
}

export default Pages;