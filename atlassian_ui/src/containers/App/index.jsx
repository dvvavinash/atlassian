import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'


import Pages from '../Pages';

import './styles.scss';


class App extends React.Component {
  render() {
    return (
      <div className="app-container">
        <Router>
          <Switch>
            <Route exact path="/spaceexpolorer/:id" render={(props) => (
              <Pages key={props.match.params.id} {...props} />)
            }/>
            <Route path='/' exact={true} render={(props) => ( // to redirect to the spaceexpolorer page
              <Pages key={props.match.params.id} {...props} />)
            }/>
          </Switch>
        </Router>
      </div>
    )
  }
}
export default App;