
  
**Start both Frontend and Backend at same time**
1. `npm install`
2. `npm start`

after successfull server starting you will get below message

```
  Starting up http-server, serving atlassian_ui/build
  Available on:
    http://127.0.0.1:8080
    http://your ip:8080
  Hit CTRL-C to stop the server
  server is listens at 3001
```
open `http://localhost:8080` to see results
